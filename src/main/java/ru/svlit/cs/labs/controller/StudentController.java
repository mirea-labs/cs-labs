package ru.svlit.cs.labs.controller;

import org.springframework.web.bind.annotation.*;
import ru.svlit.cs.labs.dao.StudentRepository;
import ru.svlit.cs.labs.model.Student;

import java.util.List;

@RestController
public class StudentController {
    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @GetMapping("/student/{id}")
    public Student getById(@PathVariable int id) {
        return studentRepository.get(id);
    }

    @GetMapping("/student")
    public List<Student> getAll() {
        return studentRepository.getAll();
    }

    @GetMapping("/student/group/{id}")
    public List<Student> getByGroupId(@PathVariable int id) {
        return studentRepository.getByGroupId(id);
    }

    @DeleteMapping("/student/{id}")
    public Student delete(@PathVariable int id) {
        return studentRepository.delete(id);
    }

    @PostMapping("/student")
    public Student add(@RequestBody Student student) {
        return studentRepository.add(student);
    }

    @PutMapping("/student/{id}")
    public Student edit(@PathVariable int id, @RequestBody Student student) {
        return studentRepository.edit(id, student);
    }
}
