package ru.svlit.cs.labs.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.svlit.cs.labs.dao.MarkRepository;
import ru.svlit.cs.labs.model.Mark;

@RestController
public class MarkController {
    private final MarkRepository markRepository;

    public MarkController(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    @GetMapping("/mark/{id}")
    public Mark getMark(@PathVariable int id) {
        return markRepository.get(id);
    }
}
