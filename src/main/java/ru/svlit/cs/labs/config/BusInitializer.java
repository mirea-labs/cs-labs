package ru.svlit.cs.labs.config;

import net.progruzovik.bus.replication.Replicator;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import ru.svlit.cs.labs.dao.StudentRepository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;

@Component
@DependsOn("busHandler")
public class BusInitializer {

    private final Replicator replicator;
    private final StudentRepository studentJdbc;

    public BusInitializer(Replicator replicator, StudentRepository studentJdbc) {
        this.replicator = replicator;
        this.studentJdbc = studentJdbc;
    }

    @PostConstruct
    public void init() throws IOException {
        initEntity("STUDENT", studentJdbc.getAllLocal());
    }

    private <T> void initEntity(String name, List<T> data) throws IOException {
        replicator.initializeEntity(name);
        for (T row : data) {
            replicator.addRow(name, row);
        }
    }
}
