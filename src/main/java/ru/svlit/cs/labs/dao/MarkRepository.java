package ru.svlit.cs.labs.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import ru.svlit.cs.labs.model.Mark;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class MarkRepository {
    private final JdbcTemplate jdbcTemplate;

    public MarkRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Mark get(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM MARK WHERE id = ?", this::mapMark, id);
    }

    @Nullable
    public Mark search(@NonNull String markName) {
        return jdbcTemplate.queryForObject("SELECT * FROM mark WHERE name = ?", Mark.class, markName);
    }

    @NonNull
    private Mark mapMark(ResultSet resultSet, int i) throws SQLException {
        return new Mark(
                resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getString("value")
        );
    }
}
